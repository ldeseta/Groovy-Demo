/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dosideas.groovy

//definimos un closure "cuadrado" y lo invocamos
cuadrado = {it * it}
println cuadrado(9)

//devuelve un array con los cuadrados de todos los elementos
[ 1, 2, 3, 4 ].collect(cuadrado)

//igual al anterior, e imprimos el resultado con un closure anónimo
[ 1, 2, 3, 4 ].collect(cuadrado).each({println it})

//un closure con varios parámetros
imprimirMap = { clave, valor -> println clave + " es " + valor }
[ "Zim" : "invasor", "Dib" : "humano", "Gaz" : "humana" ].each(imprimirMap)

//sumando valores...
otroMap = ["asdf": 1 , "qwer" : 2, "sdfg" : 10]
suma = 0
otroMap.keySet().each( { suma += otroMap[it] } )
println suma

