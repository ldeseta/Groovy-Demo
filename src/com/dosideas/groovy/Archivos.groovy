/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosideas.groovy

//abrimos un archivo
archivo = new File("ejemplo.txt")

//dejamos el archivo en blanco
archivo.write("");

//le agregamos 10 lineas de texto
10.times( { archivo.append("linea $it\n") } )

//leemos el archivo
archivo.eachLine( { println it } )
