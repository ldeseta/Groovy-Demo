//Este Script necesita que esté levantada la base de datos JavaDB de NetBeans
//junto a su esquema de ejemplo "travel" (que viene de manera predeterminada)
//Para iniciar JavaDB desde NetBeans:
//    1. ir al menú Window > Services
//    2. Abrir el noco Databases
//    3. Click derecho en Java DB > Start Server
package com.dosideas.groovy

import groovy.sql.Sql

//creamos la instancia Sql que representa la conexión a la base
sql = Sql.newInstance("jdbc:derby://localhost:1527/travel", "travel", "travel", "org.apache.derby.jdbc.ClientDriver")

//ejecutamos un SQL y por cada fila ejecutamos un closure
sql.eachRow("select * from PERSON", { println "ID: ${it.personId}, Nombre: ${it.name}"} );

//ejecutamos un SQL y obtenemos sólo la primer fila
//ademas usamos alias a en las columnas, y luego los usamos en los nombres
//de las variables
row = sql.firstRow("select personid as id, name as nombre from PERSON")
println "Columna personid = ${row.id} y la columna nombre = ${row.nombre}"


id = 9999
nombre = "Invasor Zim"

//Podemos ejecutar querys complejos, como ser borrar un registro
sql.execute("delete from PERSON where personid = ?", [id])

//O insertar...
sql.execute("insert into PERSON (personid, name) values (?,?)", [id, nombre])

//y lo borramos otra vez...
sql.execute("delete from PERSON where personid = ?", [id])

