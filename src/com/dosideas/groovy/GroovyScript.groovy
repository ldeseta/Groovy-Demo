/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dosideas.groovy

assert "invasor" ==~ /invasor/

//el caracter ? indica que la letra anterior es opcional
assert "perro" ==~ /perros?/
assert "perros" ==~ /perros?/

//se pueden usar parántesis para agrupar expresiones. En este caso, todo el
//texto "es" es opcional
assert "invasor" ==~ /invasor(es)?/
assert "invasores" ==~ /invasor(es)?/

//El operador =~ busca una ocurrencia del patron en el string
//El operador ==~ iguala el patrón al string.
assert "El invasor Zim es del Imperio Irken" =~ /invasor/
assert ("El invasor Zim es del Imperio Irken" ==~ /invasor/ )== false

//también puede usarse programáticamente
def pattern =  ~/Zim/
def matcher = pattern.matcher('El invasor Zim es del Imperio Irken')
assert matcher.find()  //find() es equivalente a =~
assert matcher.matches() == false  //matches() es equivalente a ==~



