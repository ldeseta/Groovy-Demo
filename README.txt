﻿============================================
             Tutorial de Groovy
                    http://www.dosideas.com
============================================

Ejemplos varios que demuestran el uso de Groovy.

El proyecto consiste en varios ejemplos aislados, cada uno en un script
independiente. Cada script contiene varios ejemplos de uso de las distintas
características de Groovy.

Para ejecutar el script, abrirlo con NetBeans y ejecutar el archivo (Shift + F6)


 Contenido
===========
 src/com/dosideas/groovy  : paquete de la demo. Aquí hay varios scripts Groovy
                            que pueden ejecutarse de forma independiente.


 Preparación de la base de datos 
=================================
Algunos scripts utilizan una base de datos para demostrar el uso de SQL. Estos
scripts utilizan la base Derby, que viene preconfigurada con NetBeans 6.x


 Más información
=================
http://www.dosideas.com/cursos/course/view.php?id=9
